import React from 'react';

import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Camera from './src/Screens/Camera';
import HomeScreen from './src/Screens/HomeScreen';
import ResultScreen from './src/Screens/ResultScreen'
import Libarary, { Library } from './src/Screens/Library'
import Options from './src/Screens/Options'

const navigator = createStackNavigator(
  {
    MyCamera : Camera , 
    Home : HomeScreen  , 
    Result : ResultScreen ,
    MyLibrary : Library , 
    Options : Options ,
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      title: "App"
    }
  }
);

export default createAppContainer(navigator);
