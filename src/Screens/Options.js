import React, {useState , useEffect} from 'react';
import { Alert ,StyleSheet, Text, View, Image , Dimensions, ActivityIndicator , FlatList ,
TouchableOpacity } from 'react-native';
import { ListItem , Card , Icon , Button} from 'react-native-elements'

import NavigationBar from './../Components/NavigationBar'

const {width , height} = Dimensions.get('window');
//780,411
const navigationBarHeight = 0.09 * height;

const list = [
    'Read me' ,
    'Feedback' , 
    'About' ,
    'Rate us'
]
export const Options = (props) => {
    const Item = ({title}) => (
        <View style={{flexDirection: 'column'}}>
            <TouchableOpacity style={styles.touch}>
            <View style={styles.card}>
                <Text style={styles.text}>{title}</Text>
            </View>
            </TouchableOpacity>
        </View>
    );

    const renderItem = ({ item }) => (
        <Item title={item} />
        );
    return (
        <View style={styles.page}>
            <View style={styles.resultContainer}>
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    keyExtractor={item => item.title}
                />
            </View>
            
            <NavigationBar properties={props}/>
        </View>

  );
}

const styles = StyleSheet.create({
    page : {
        height : height , 
        width : width , 
        backgroundColor : '#F8F8F8' ,
        marginTop:0 
        },
    resultContainer : {
        height : height - navigationBarHeight - 50 , 
        width : width,
        // flexDirection : 'row'
    },
    card : {
        width: width*2/3 , 
        backgroundColor : '#EEEFF1' , 
        height : 75 , 
        borderRadius : 15 , 
        marginLeft : width/6 ,
        marginTop : 40, 
        marginBottom : 5 ,
        alignItems:"center" , 
        justifyContent : 'center' ,
        elevation:3 ,
    }, 
    text:{
        fontSize : 20
    },
});

export default Options;
