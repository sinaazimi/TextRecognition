
import React, {useState} from 'react';
import {View , Text , StyleSheet , FlatList ,Image , Dimensions , TouchableOpacity, TextInput ,Alert , ScrollView} from 'react-native'
import NavigationBar from '../Components/NavigationBar'
import ControlBar from '../Components/ControlBar'
import Camera from "./Camera";
import { Icon } from 'react-native-elements'
import ResultHandle from './../Components/ResultHandle'
import ControBar from './../Components/ControlBar'

const {width , height} = Dimensions.get('window');
const navigationBarHeight = 0.09 * height;


const ResultScreen = (props) => {
    const [pick,setPick] = useState(null)
    let recognizedText = ''
    let uri = ''
    let mode = ''
    let date = ''
    try{
      recognizedText =  props.navigation.state.params.Text
      uri = props.navigation.state.params.Uri
      mode = props.navigation.state.params.mode
      date = props.navigation.state.params.date

    }catch(err){
      console.error(err);
      recognizedText ='';
    }

    return (
    <>
    <View style={styles.page}>
        <View style={styles.resultContainer}>
          <TouchableOpacity>
            <Image source = {{uri: uri,  
                        width: 200, 
                        height: 200}}
                  style={{marginLeft: (19*width/40) - 100 , marginTop : 20}}            
            />
        </TouchableOpacity>
            <ScrollView style={styles.resultTextContainer} showsVerticalScrollIndicator={false} >
            <Text style={styles.result}>{recognizedText}</Text>
            </ScrollView>
        </View> 
        <ResultHandle result={recognizedText} image={uri} mode={mode} date={date} navigation={props.navigation}/>
        <NavigationBar properties={props}/>
    </View>
    
    </>
    );
};

const styles = StyleSheet.create({
    page : {
      height : height , 
      width : width , 
      backgroundColor : '#F8F8F8' ,
      position : 'absolute' , 
      top : 0 ,
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor : '#F8F8F8' ,
      
    },

    libraryIcon : {
    },
    moreIcon : {
    
    },
    scanIcon :{
      marginLeft :width/4.11 , 
      marginRight :width/4.11 , 
      marginBottom : height/7.79

    },    
    resultContainer : {
        height : height/1.8 , 
        width : width - width/20 ,
        marginLeft : width/40 ,
        marginTop : width/15 , 
        // borderColor : '#27BC62' , 
        // borderColor : '#EEEFF1' ,
        // borderWidth : 4 , 
        borderRadius :20 , 
        shadowColor : 'red'  , 
        shadowOffset : {height:5,width:5} ,
        shadowOpacity:1,
        shadowRadius:5,
        elevation:3 ,
        backgroundColor : '#EEEFF1' ,
    }, 
    result:{
      fontSize : 18 , 
      textAlign : 'center'
    }, 
    resultTextContainer:{
      marginLeft : (19*width/40) - 150 , 
      width :300 ,
      marginTop : 10 , 
      
    }
});

export default ResultScreen