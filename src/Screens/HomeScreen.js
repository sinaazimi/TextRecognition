
import React, {useState} from 'react';
import {View , Text , StyleSheet , FlatList ,Image , Dimensions , TouchableOpacity, Alert} from 'react-native'
import NavigationBar from '../Components/NavigationBar'
import ControlBar from '../Components/ControlBar'
import Camera from "./Camera";
import { Icon } from 'react-native-elements'

const {width , height} = Dimensions.get('window');
const navigationBarHeight = 0.09 * height;

const HomeScreen = (props) => {
    const [pick,setPick] = useState(null)
    return (
    <View style={styles.page}>
        <View style={styles.cameraContainer}>
            <Camera properties={props}/>
        </View>

        
        <NavigationBar properties={props}/>
    </View>
    );
};

const styles = StyleSheet.create({
    textStyle:{
        marginVertical :50 ,
    },
    text: {
        fontSize: 30
      },
      page : {
        height : height , 
        width : width , 
        backgroundColor : '#F8F8F8' ,
        position : 'absolute' , 
        top : 0 ,
      },
      libraryIcon : {
      },
      moreIcon : {
      
      },
      scanIcon :{
        marginLeft :width/4.11 , 
        marginRight :width/4.11 , 
        marginBottom : height/7.79

      },    
      cameraContainer : {
          backgroundColor : 'black' ,
          height : height - navigationBarHeight - 50  , 
          width : width ,  
          
      }
});

export default HomeScreen