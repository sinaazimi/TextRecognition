import React, {useState , useEffect} from 'react';
import {Button, Alert ,StyleSheet, Text, View, Image , Dimensions, ActivityIndicator} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import ControlBar from './../Components/ControlBar'

import Spinner from 'react-native-loading-spinner-overlay';

import TesseractOcr, {
  LANG_ENGLISH,
} from 'react-native-tesseract-ocr';

const DEFAULT_HEIGHT = 100;
const DEFAULT_WIDTH = 100;
const defaultPickerOptions = {
  cropping: true,
  // height: DEFAULT_HEIGHT,
  // width: DEFAULT_WIDTH,
};

const {width , height} = Dimensions.get('window');

export const Camera = (props) => {

  const cameraProps =  props.properties

  const [progress, setProgress] = useState(0);
  const [imgSrc, setImgSrc] = useState(null);
  const [text, setText] = useState('');
  const [opacity,setOpacity] = useState(1);
  const [load,setLoad] = useState(false)
 

  async function recognizeTextFromImage (path) {
    setLoad(true)
    setImgSrc(path)
    try { 
      const tesseractOptions = {};
      let uri = path.replace('file://', '');
      const recognizedText = await TesseractOcr.recognize(
        uri,
        'LANG_ENGLISH',
        tesseractOptions,
      );
      setText(recognizedText);
      cameraProps.navigation.navigate('Result',{Text: recognizedText , Uri: path , mode:'new'})
      
      
    } catch (err) {
      console.error(err);
      setText('');
    }

    setLoad(false)
    setProgress(0);
    
  };

  async function recognizeFromPicker (options = defaultPickerOptions)  {
    
    try {
      setLoad(true)
      const image = await ImagePicker.openPicker(options);
      setImgSrc({uri: image.path});
      
      await recognizeTextFromImage(image.path);
    } catch (err) {
      if (err.message !== 'User cancelled image selection') {
        console.error(err);
      }
    }
    setLoad(false)
  };

  async function recognizeFromCamera (options = defaultPickerOptions) {
    try {
      setLoad(true)
      const image = await ImagePicker.openCamera(options);
      setImgSrc({uri: image.path});
      await recognizeTextFromImage(image.path);
    } catch (err) {
      if (err.message !== 'User cancelled image selection') {
        console.error(err);
      }
    }
    setLoad(false)
  };

  return (
    <>
    <View style={styles.container}>   
        <Spinner
        visible={load}
        textContent={'This may take some time...'}
        textStyle={styles.spinnerTextStyle}
        color='black'
        overlayColor = 'rgba(238, 239, 241, 0.85)'
        size = 'large'
        textStyle = {styles.spinnerText}
        />
        <Image source={require('./../Assets/ocr2.png')} style={styles.ocrPic}></Image>
        <Text style={styles.instructions}>Select an image source :</Text>        
    </View>
    <ControlBar gallery={recognizeFromPicker} camera={recognizeFromCamera} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor : '#F8F8F8' ,
    
  },
  options: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  button: {
    marginHorizontal: 10,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
    position : 'absolute' , 
    bottom  : height/4
  },
  ocrPic : {
    resizeMode : 'contain' , 
    width : height/2.4 , 
    height : height/2.4 , 
    position  : 'absolute' , 
    top : 30 ,
  }, 
  indicator : {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center' , 
    zIndex : 4 ,
  },
});

export default Camera;