import React from "react";
import { Text, StyleSheet ,View , Button , Image, TouchableOpacity  , Dimensions} from "react-native";
import { Icon } from 'react-native-elements'

const {width , height} = Dimensions.get('window');

const navigationBarHeight = 0.09 * height;

const NavigationBar = (props) => {
  const barProps =  props.properties

  return(
      <View style={styles.navigationBar}>

        <Icon
            name='ellipsis-horizontal-outline'
            type='ionicon' 
            size={40}
            containerStyle={styles.moreIcon}
            onPress={() => {barProps.navigation.navigate('Options')}}
        />
        <Icon
            name='barcode-outline'
            type='ionicon' 
            size={40}
            iconStyle={styles.scanIcon}
            containerStyle={styles.scanIconCont}
            color = '#27BC62'
            onPress={() => {barProps.navigation.navigate('Home')}}
            raised={true}
        />
        <Icon
            name='grid-outline'
            type='ionicon' 
            size={40}
            containerStyle={styles.libraryIcon}
            onPress={() => {barProps.navigation.navigate('MyLibrary')}}
            
        />


      </View>
  );
};

const styles = StyleSheet.create({
  navigationBar :{
    backgroundColor : '#EEEFF1' , 
    width : width, 
    height : navigationBarHeight, 
    position : 'absolute' , 
    bottom : 50 ,
    alignItems : 'center' , 
    justifyContent : 'center' ,
    flexDirection : 'row' ,
    elevation:3 ,
  }, 
  libraryIcon : {
    marginLeft : width/5.7
  },
  moreIcon : {
    marginRight : width/5.7
  },
  scanIcon :{
    // marginLeft :width/4.11 , 
    // marginRight :width/4.11 , 
    // marginBottom : height/7.79
    // marginBottom : navigationBarHeight
  }, 
  scanIconCont : {
    marginBottom : navigationBarHeight
  }

});

export default NavigationBar;
